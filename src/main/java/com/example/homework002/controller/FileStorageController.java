package com.example.homework002.controller;

import com.example.homework002.model.DataTable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Controller
public class FileStorageController {
    @PostMapping("/create")
    public String index(@RequestParam("file") MultipartFile file, @ModelAttribute DataTable dataTable){
        dataTable.setId(HomeController.listActicle.size()+1);
        dataTable.setImage(file.getOriginalFilename());
        HomeController.listActicle.add(dataTable);
        try{
            String fileName =file.getOriginalFilename();
            Path path = Paths.get("src/main/resources/static/image/"+fileName);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
        }
        return "redirect:/";
    }
}
