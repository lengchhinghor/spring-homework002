package com.example.homework002.controller;

import com.example.homework002.model.DataTable;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Controller
public class UpdateController {
//    @GetMapping("/editActical")
//    public String getEdit(){
//        return "redirect:/";
//    }
    int newId;
    @PostMapping("/edit")
    public String index(@RequestParam("file") MultipartFile file, @ModelAttribute DataTable dataTable){
        dataTable.setId(HomeController.listActicle.size()+1);
        dataTable.setImage(file.getOriginalFilename());
        for (int i=0; i<HomeController.listActicle.size(); i++){
            if (newId == HomeController.listActicle.get(i).getId()){
                HomeController.listActicle.get(i).setId(newId);
                HomeController.listActicle.get(i).setTitle(dataTable.getTitle());
                HomeController.listActicle.get(i).setDescription(dataTable.getDescription());
                HomeController.listActicle.get(i).setImage(dataTable.getImage());
            }
        }
        try{
            String fileName =file.getOriginalFilename();
            Path path = Paths.get("src/main/resources/static/image/"+fileName);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
        }
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String getEdit(@PathVariable("id") int id, Model model){
        DataTable dataTable = null;
        newId = id;
        for (int i=0; i<HomeController.listActicle.size(); i++){
            if (id == HomeController.listActicle.get(i).getId()){
                dataTable = HomeController.listActicle.get(i);
            }
        }
        model.addAttribute("aditActical", dataTable );
        return "editActicle";
    }
}
