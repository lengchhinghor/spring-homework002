package com.example.homework002.controller;

import com.example.homework002.model.DataTable;
import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class HomeController {

   public static List<DataTable> listActicle = new ArrayList<>();
    {
        listActicle.add(new DataTable(1,"chhinghor","Description","1png.png"));
        listActicle.add(new DataTable(2,"chhaihong","Description","2.jpg"));
        listActicle.add(new DataTable(3,"kaikeo","Description","3.jpg"));
    }
    @GetMapping("/")
    public String homePage(ModelMap modelMap){
//        model.addAttribute("dataTable",Arrays.asList(
//                new DataTable(1,"chhinghor","Description","image/1png.png")
//        ));
    modelMap.addAttribute("dataTable",listActicle);
    return "/home";
    }
    @GetMapping("/create")
    public String createPage(@ModelAttribute DataTable dataTable, ModelMap modelMap){
        modelMap.addAttribute("acticle", dataTable);
        return "createActicle";
    }

    @RequestMapping("/view/{id}")
    public String getView(@PathVariable("id") int id, Model model){
        DataTable dataTable = null;
        for (int i=0; i<listActicle.size(); i++){
            if (id == listActicle.get(i).getId()){
                dataTable = listActicle.get(i);
            }
        }
        model.addAttribute("view", dataTable);
        return "viewActicle";
    }

    @RequestMapping("/delete/{id}")
    public String deleteItem(@PathVariable("id") int id){
        for (int i=0; i<listActicle.size(); i++){
            if (id == listActicle.get(i).getId()){
               listActicle.remove(listActicle.get(i));
            }
        }
        return "redirect:/";
    }
}
